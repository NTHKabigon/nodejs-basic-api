const mgdb = require('../services/mongodb')

var conn = mgdb.db('tutorial-db').collection('tutorials')
var ObjectId = require('mongodb').ObjectId

function TutorialMongoClientController() {
    const SELF = {};
    return {
        create: async (req, res, next) => {
        
            try {
                /** Get data input */
                let { title, desc} = req.body;
            

                /** validate param*/
                if (!title) {
                    return res.status(400).send({
                        status: "Input param cannot empty",
                        data: null
                    })
                }

                /** Save database */
                const doc = { title: title, description: desc };
                const result = await conn.insertOne(doc)
                
                if (!result.insertedId) {
                    return res.status(400).send({
                        message: "Add tutorial fail",
                        data: null
                    })
                }

                return res.status(200).send({
                    message: "success",
                    data: null
                });
            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        findAll: async (req, res, next) => {
            try {
                /** Get data input */
                let title = req.query.title;
                let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

                /** Get all tutorial */
                let tutorials = await conn.find(condition).toArray()

                return res.status(200).send({
                    message: "success",
                    data: tutorials
                })

            } catch (error) {
                return res.status(500).send({
                        message: error.message,
                        data: null
                });
            }
        },
        findById: async (req, res, next) => {
            try {
                /** Get data input */
                let id = req.params.id;
                /** validate params */
                if (!id) {
                    return res.status(400).send({
                        message: "Input param cannot empty",
                        data: null
                    })
                }

                /** Get tutorial by id */
                let tutorial = await conn.findOne({_id: ObjectId(id)})
                if (!tutorial){
                    return res.status(404).send({
                        message: `tutorial id: ${id} not found`,
                        data: null
                    })
                }
                return res.status(200).send({
                    message: `success`,
                    data: tutorial
                })

            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        findAllPublished: async (req, res, next) => {
            try {
                /** Get tutorial published */
                let tutorials = await conn.find({published:true})
                if (!tutorials){
                    return res.status(404).send({
                        message: `tutorials not found`,
                        data: null
                    })
                }
                return res.status(200).send({
                    message: `success`,
                    data: tutorials
                })

            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        update: async (req, res, next) => {
            try{
                /** Get data input */
                let { title, desc, published} = req.body;
                let id = req.params.id;

                /** validate input*/
                if (!id) {
                    return res.status(400).send({
                        message: "Input param cannot empty",
                        data: null
                    })
                }

                /** Check tutorial exist */
                let tutorial = await conn.findOne({_id:ObjectId(id)})
                if(!tutorial) {
                    return res.status(404).send({
                        message: "tutorial not found",
                        data: null
                    })
                }

                /** Binding data update */
                if(title){
                    tutorial.title = title
                }
                if(desc){
                    tutorial.description = desc
                }
                tutorial.published = published !== undefined ? published : tutorial.published
            
                /** Save database */
                const rs = await conn.updateOne({_id:ObjectId(id)}, {"$set":tutorial})
                return res.status(200).send({
                    message:"Update success",
                    data: null
                })
            }catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        deleteById: async (req, res, next) => {
               /** Get data input */
               let id = req.params.id;

                /** validate input*/
               if (!id) {
                   return res.status(400).send({
                    message: "Input param cannot empty",
                       data: null
                   })
               }

               /** Check tutorial exist */
               let tutorial = await conn.findOne({_id: ObjectId(id)})
               if(!tutorial) {
                   return res.status(404).send({
                    message: "tutorial not found",
                       data: null
                   })
               }

               /** Delete tutorial */
              const rs = await conn.deleteOne({_id:ObjectId(id)})
               return res.status(200).send({
                message:"Delete success",
                    data: rs  
                })
           
        },
        deleteAll: async (req, res, next) => {
            try {
               /** Delete tutorial */
              const rs = await conn.deleteMany({})
               return res.status(200).send({
                message:"Delete success",
                    data: null  
                })
            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
    };
}
  
module.exports = new TutorialMongoClientController();
  