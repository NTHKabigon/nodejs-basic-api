    const tutorialMongooseController = require("../controllers/tutorial_mongoose");
    const tutorialMongoClientController = require("../controllers/tutorial_mongo_client");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    /**/
    router.post("/api-mongoose/tutorials", tutorialMongooseController.create);
    /**/
    router.post("/api-mongo-client/tutorials", tutorialMongoClientController.create);
  
    // Retrieve all Tutorials
    /**/
    router.get("/api-mongoose/tutorials", tutorialMongooseController.findAll);
    /**/
    router.get("/api-mongo-client/tutorials", tutorialMongoClientController.findAll);
  
    // Retrieve all published Tutorials
    /**/
    router.get("/api-mongoose/tutorials/published", tutorialMongooseController.findAllPublished);
    /**/
    router.get("/api-mongo-client/tutorials/published", tutorialMongoClientController.findAllPublished);
  
    // Retrieve a single Tutorial with id
    /**/
    router.get("/api-mongoose/tutorials/:id", tutorialMongooseController.findById);
    /**/
    router.get("/api-mongo-client/tutorials/:id", tutorialMongoClientController.findById);
  
    // Update a Tutorial with id
    /**/
    router.put("/api-mongoose/tutorials/:id", tutorialMongooseController.update);
    /**/
    router.put("/api-mongo-client/tutorials/:id", tutorialMongoClientController.update);
  
    // Delete a Tutorial with id
    /**/
    router.delete("/api-mongoose/tutorials/:id", tutorialMongooseController.deleteById);
    /**/
    router.delete("/api-mongo-client/tutorials/:id", tutorialMongoClientController.deleteById);
  
    // Delete all Tutorials
    /**/
    router.delete("/api-mongoose/tutorials", tutorialMongooseController.deleteAll);
    /**/
    router.delete("/api-mongo-client/tutorials", tutorialMongoClientController.deleteAll);

    module.exports = router;
