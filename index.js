const express = require('express');
const app = express();
const router = require(`./app/routers/tutorial.routes`);
const mongoose = require("mongoose")
require('dotenv').config()
mongoose.connect("mongodb://root:root@127.0.0.1:27017/tutorial-db",
  {
    authSource:"admin",
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
).then(()=>{
  console.log("Connected to the mongodb!");

}).catch(err => {
  console.log("Connect to the database error: ", err);
})

app.use(express.json())

app.use("/", router)

app.listen(process.env.PORT_LOCAL);

console.log('RESTful API server started on: ' + process.env.PORT_LOCAL);
