const tutorialModel = require('../models/tutorial.model');

function TutorialMongoController() {
    const SELF = {};
    return {
        create: async (req, res, next) => {
            try {
                /** Get data input */
                let { title, desc} = req.body;

                /** validate param*/
                if (!title) {
                    return res.status(400).send({
                        status: "Input param cannot empty",
                        data: null
                    })
                }

                console.log("tutorialModel: ",tutorialModel);

                /** Save database */
                let tutorialData = new tutorialModel({
                    title: title,
                    description: desc,
                    published: true
                }) 

                console.log("tutorialData: ",tutorialData);

                let newTutorial = await tutorialData.save()
                if (!newTutorial._id) {
                    return res.status(400).send({
                        message: "Add tutorial fail",
                        data: null
                    })
                }

                return res.status(200).send({
                    message: "success",
                    data: newTutorial
                });
            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        findAll: async (req, res, next) => {
            try {
                /** Get data input */
                let title = req.query.title;
                let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

                /** Get all tutorial */
                let tutorials = await tutorialModel.find(condition).catch(err => {
                    console.log("Get all tutorial error: ", err);
                })

                return res.status(200).send({
                    message: "success",
                    data: tutorials
                })

            } catch (error) {
                return res.status(500).send({
                        message: error.message,
                        data: null
                });
            }
        },
        findById: async (req, res, next) => {
            try {
                /** Get data input */
                let id = req.params.id;
                /** validate params */
                if (!id) {
                    return res.status(400).send({
                        message: "Input param cannot empty",
                        data: null
                    })
                }

                /** Get tutorial by id */
                let tutorial = await tutorialModel.findById(id)
                if (!tutorial){
                    return res.status(404).send({
                        message: `tutorial id: ${id} not found`,
                        data: null
                    })
                }
                return res.status(200).send({
                    message: `success`,
                    data: tutorial
                })

            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        findAllPublished: async (req, res, next) => {
            try {
                /** Get tutorial published */
                let tutorials = await tutorialModel.find({published:true})
                if (!tutorials){
                    return res.status(404).send({
                        message: `tutorials not found`,
                        data: null
                    })
                }
                return res.status(200).send({
                    message: `success`,
                    data: tutorials
                })

            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        update: async (req, res, next) => {
            try{
                /** Get data input */
                let { title, desc, published} = req.body;
                let id = req.params.id;

                /** validate input*/
                if (!id) {
                    return res.status(400).send({
                        message: "Input param cannot empty",
                        data: null
                    })
                }

                /** Check tutorial exist */
                let tutorial = await tutorialModel.findById(id)
                if(!tutorial) {
                    return res.status(404).send({
                        message: "tutorial not found",
                        data: null
                    })
                }

                /** Binding data update */
                if(title){
                    tutorial.title = title
                }
                if(desc){
                    tutorial.description = desc
                }
                tutorial.published = published !== undefined ? published : tutorial.published
            
                /** Save database */
                await tutorialModel.updateOne({_id:id}, tutorial)
                return res.status(200).send({
                    message:"Update success",
                    data: null
                })
            }catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        deleteById: async (req, res, next) => {
            try {
               /** Get data input */
               let id = req.params.id;

                /** validate input*/
               if (!id) {
                   return res.status(400).send({
                    message: "Input param cannot empty",
                       data: null
                   })
               }

               /** Check tutorial exist */
               let tutorial = await tutorialModel.findById(id)
               if(!tutorial) {
                   return res.status(404).send({
                    message: "tutorial not found",
                       data: null
                   })
               }

               /** Delete tutorial */
               await tutorialModel.deleteOne({_id: tutorial._id})
               return res.status(200).send({
                message:"Delete success",
                    data: null  
                })
            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
        deleteAll: async (req, res, next) => {
            try {

               /** Delete tutorial */
               await tutorialModel.deleteMany({})
               return res.status(200).send({
                message:"Delete success",
                    data: null  
                })
            } catch (error) {
                return res.status(500).send({
                        status: "Internal error",
                        data: null
                    })
            }
        },
    };
}
  
module.exports = new TutorialMongoController();
