const { MongoClient } = require("mongodb");
require('dotenv').config()

// Connection URI
const uri = process.env.URI_MONGODB
// Create a new MongoClient
const client = new MongoClient(uri);

async function run() {
  try {
    // Connect the client to the server (optional starting in v4.7)
    await client.connect();
    console.log("Connected successfully to mongodb");
  }catch(err) {
    console.log("Error connecting to mongodb error:", err);
  }
}
    run().catch(console.dir)

const mgdb = client

module.exports = mgdb
