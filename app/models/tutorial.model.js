const mongoose = require("mongoose");

const tutorialSchema = new mongoose.Schema({
    title: { type: String},
    description: { type: String },
    published: { type: Boolean},
},{
    timestamps: true
});
  
module.exports = Tutorial = mongoose.model('tutorial', tutorialSchema)
  